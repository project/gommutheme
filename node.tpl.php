<?php
?>

<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?>
        <?php if (!$status) { print ' node-unpublished'; } ?> clear-block <?php if ($page) { print ' zacPage'; } else { print ' zacTeaser'; } ?>">

<?php if (strlen($picture) > 32) print $picture ?>

<?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
  <div class="meta">
    <span class="submitted"><?php print $submitted ?></span>
  </div>
  <?php endif; ?>

  <div class="content">
    <?php print $content ?>
  </div>

  <?php if ($terms && $page): ?>
    <div class="terms"><?php print $terms ?></div>
  <?php endif;?>

  <?php print $links; ?>
</div>