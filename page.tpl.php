<?php

/**
 * @file
 * Main page template.
 *
 * Main template for all pages in gommutheme.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
  xml:lang="<?php print $language->language ?>"
  lang="<?php print $language->language ?>"
  dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
<!--[if lt IE 7]>
  <link type="text/css" rel="stylesheet" media="all" href="/<?php print $directory.'/'; ?>ie6fix.css" />
<![endif]-->
<script type="text/javascript">
  $(document).ready( function(){ gummuthemeLoadReady(); });
</script>
</head>

<body>

<div id="sizeframe">

  <div id="l_top" class="cb">
    <div id="c_sitelogo" class="l_mr l_mb l_w2">
      <?php 
      // render logo, title and slogan
      $ptitle = trim(@check_plain($site_name) .' '. @check_plain($site_slogan));
      print '<a class="homelink noborder" href="'. check_url($front_page) .'" title="'. $ptitle .'">';
      if ($logo) {
        print '<img src="'. check_url($logo) .'" alt="'. $ptitle .'" id="logo" class="logo" />';
      } 
      else {
        print '<span class="homename">'. check_plain($site_name) .'</span>';
        if ($site_slogan) {
          print '<br /><span class="homeslogan">'. check_plain($site_slogan) .'</span>';
        }
      }
      print '</a>';
      ?>    
    </div>
    <div id="c_topright" class="l_mb">
      <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'primmenu')) ?><?php } ?>
      <div id="shinyhead"></div>
      <?php print $header_right; ?>
    </div><div class="clear-block"></div>
  </div>
  
  <div id="l_mid" class="cb">
  
    <div id="l_midleft" class="l_mr l_w3">
      <?php if ($show_messages && $messages): print '<div class="msgs l_mb">'. $messages .'</div>'; endif; ?>
      <?php if ($tabs) print $tabs; ?>
      <?php if ($content_top): print '<div id="content_top" class="l_mb l_w3">'. $content_top .'</div>'; endif; ?>
      <div id="content" class="l_mb l_w3 <?php if ($node->type) print 'mc-'. $node->type; ?>">
        <?php if ($title && ($node->type != "anwalt")): print "<h1 class='mainTitle'>$title</h1>"; endif; ?>
        <?php print $help; ?>
        <?php print $content; ?>
      </div>
      <?php if ($content_bottom): print '<div id="content_bottom" class="l_mb l_w3">'. $content_bottom .'</div>'; endif; ?>
      <?php if (($content_left) || ($content_right)) { 
              print '<div id="content_left" class="l_mb l_mr l_w2">'. $content_left .'</div>'; 
              print '<div id="content_right" class="l_mb l_w2">'. $content_right .'</div>'; 
            } ?>
    </div> <!-- of #l_midleft -->
    
    <div id="l_midright" class="l_w2">
      <?php if ($fullright||$mission) { print '<div id="fullright" class="l_mb l_w2">';
          if ($mission) { print '<div id="mission">'. $mission.'</div>'; }
          print $fullright .'</div>'; } ?>
      <?php if ($left): print '<div id="left" class="l_mb l_w1">'. $left .'</div>'; endif; ?>
      <?php if ($right): print '<div id="right" class="l_mb l_mr l_w1">'. $right .'</div>'; endif; ?>
    </div>
    
  </div> <!--  of #l_mid -->
  
  <div id="l_bottom" class="cb">
    <div id="footer" class="l_mb">
      <?php print $footer; ?>
    </div>
    <div id="footer_left" class="l_mb l_mr l_w2 cl">
      <?php print $footer_left; ?>
    </div>
    <div id="footer_center" class="l_mb l_mr l_w2"><?php print $footer_center; ?></div>
    <div id="footer_right" class="l_mb l_w2"><?php print $footer_right; ?></div>
    <div id="footer-msg" class="l_mb">
      <?php print $footer_message; ?>
    </div>
    <div class="themeauthor"><a href="http://www.proxiss.de">Drupal Theme by proxiss GmbH</a>, 
    mini icons by brandspankingnew.</div>
  </div>
</div>

<div id="closure" class="cb">
    <?php print $closure ?>
</div>
<?php if ($before_body) print '<div class="beforeBody">'. $before_body .'</div>'; ?>
</body>
</html>


