
// gets called after load is ready and complete
function gummuthemeLoadReady() {
	if (jQuery.browser.version != "6.0") fitShiny();
}

// fit size of transparent shiny above primmenu
function fitShiny() {
	$('#shinyhead').css("width", 960 - $('#primmenu li.first').position().left - 2 - 320);
	$('#shinyhead').css("height", 0.382 * $('#primmenu li.first').innerHeight());
	$('#shinyhead').css("top", $('#primmenu li.first').position().top);
	$('#shinyhead').css("left", 2 + $('#primmenu li.first').position().left);
}